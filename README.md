# UI CGR



### MODIFICAR SOURCE.JS

```
.
├── /components/                # Componentes
│   ├── /Search/sources.js           # Archivo sources.js
│   ├── /Search/ContentPage.scss     # Estilos
│   ├── /Search/index.js             # Logica

```

### COMO INSTALAR

Descargar este repo y correr en la carpeta raiz del repo

```
$ npm install
$ npm start
```

Luego abrir [http://localhost:3000/](http://localhost:3000/) en el explorador



### Estructura sources.js

sources.js es un archivo json que permite modificar los campos del buscador

```
// ESTRUCTURA CONCEPTUAL
{
  nombre_del_key: {
    avanzado: [ //EXISTE 3 TIPO DE FILTROS AVANZADOS - category - date - free
        {type: 'category',  name: 'key_asociado_al_campo',options: ['opcion_1','opcion_2']}, // Muestra listado y fuerza resultados a pertenecer a esa categoria, si se include dynamic: true se maneja en forma dinamica
        {type: 'date', name: 'Fecha',dir: 'gt', field:'fecha_promulgación'}, // Muestra controlador de fechas
        {type: 'free', name: 'n_dictamen'}, //Muestra campo de texto que busca por "wildcard"
        {type: 'force', name: 'n_dictamen'}, //Muestra campo de texto que realiza busqueda exacta
    ],
    min: 'DICT', // NOMBRE RESSUMIDO DEL OBJETO
    result: [ /( CONTROLADOR DEL TITULO - Posee dos tipos title - date
      {type:'title',name:'n_dictamen'}, // name es el nombre del campo indexado
      {type:'date',name:'fecha_promulgación'},
      {type:'title',name:'descriptores'},

    ],
    date_name: 'fecha_documento', // Determina origen de la fecha
    highlight: 'texto', // Determina de donde se extrae el texto para el resultado
    summary: 'descriptores', // Misma función que highlight pero es de segundo nivel
    nombre: 'Dictamenes', // Nombre del boton
    doc: [ // Determina los objetos que aparecen al seleccionar el documento - existen 3 tipos - id - tab - text
      {type: 'id', name: 'n_dictamen'},
      {type: 'extended', name: 'descriptores'},
      {type: 'tab', name: 'descriptores'},
      {type: 'tab', name: 'destinatarios'},
      {type: 'tab', name: 'referencias'},
      {type: 'file', name: 'pdf'}, // INCLUYE LINK A ARCHIVO
      {type: 'tab', name: 'acción'},
      {type: 'text', name: 'texto'}, // HTML SIN PARSEAR
      {type: 'text_upper_clean', name: 'fuentes_legales'}, // TEXTO PARSEADO Y LIMPIADO EN POSICION SUPERIOR
      {type: 'text_bottom', name: 'texto'}, //TEXTO PARSEADO Y LIMPIADO
    ]
  }
}
```
