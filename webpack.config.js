var path = require('path');
var webpack = require('webpack');




const DEBUG = false;

module.exports = {
  devtool  :  DEBUG ? 'eval-source-map' : false,
  entry : [
    //'whatwg-fetch',
    'webpack-dev-server/client?http://0.0.0.0:3000',
    'webpack/hot/only-dev-server',
    __dirname
  ],
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  output: {
    path    : __dirname,
    filename: 'widget.js',
    publicPath: '/'
  },
  module: {
    loaders: [
      {
        exclude: /(node_modules)/,
        test   : /\.js?$/,
        loaders: ['react-hot', 'babel'],
        include: __dirname
      },
      {
        test: /\.scss$/,
        loaders: [
          'isomorphic-style-loader',
          `css-loader?${JSON.stringify({
            sourceMap: DEBUG,

            // CSS Modules https://github.com/css-modules/css-modules
            modules: true,
            localIdentName: DEBUG ? '[name]_[local]_[hash:base64:3]' : '[hash:base64:4]',

            // CSS Nano http://cssnano.co/options/
            minimize: !DEBUG,
          })}`,
          'postcss-loader?parser=postcss-scss',
        ],
      },
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.optimize.DedupePlugin(),
    new webpack.DefinePlugin({
    'process.env': {
      // This has effect on the react lib size
      'NODE_ENV': JSON.stringify('production'),
    },
  }),
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false,
          sequences: true,
          dead_code: true,
          conditionals: true,
          booleans: true,
          unused: true,
          if_return: true,
          join_vars: true,
          drop_console: false,
          passes: 2
        },
        sourceMap: true,
        comments: false,
        minimize: true,
        output: {
          comments: false,
        }
      }),
      new webpack.optimize.AggressiveMergingPlugin(),
      new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /es/)

  ]
};
